# AES-256-CTR
# python3 sym-crypto.py -e --key=8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92 'Билли Бонс умер.'
# python3 sym-crypto.py -d --key=8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92 0876e0013929e1490bf4fdd89b716fd2a2912bee50f82bcd6b13c7893bf7f3611c07d91306ab99c7d76445dcc1
#
# AES-128-CTR
# python3 sym-crypto.py -e --key=8d969eef6ecad3c29a3a629280e686cf 'Билли Бонс умер.'
# python3 sym-crypto.py -d --key=8d969eef6ecad3c29a3a629280e686cf 2a09ba9a944ef933fed0524a95867cefac963128f44314cbc19f088922b8a6b21f7f90eb2f8c63676d8f050a21

import os
import sys
import argparse
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend

parser = argparse.ArgumentParser(prog='python3 sym-crypto.py',
                                 description='Command line AES-CNT encoder/decoder',
                                 usage='%(prog)s [-h] ( -e | -d ) --key KEY msg',
                                 allow_abbrev=False)
parser.add_argument('-e', action='store_true', help='encrypt the message', dest='encrypt')
parser.add_argument('-d', action='store_true', help='decrypt the message', dest='decrypt')
parser.add_argument('--key', type=str, help='key for encryption/decryption', required=True, dest='key')
parser.add_argument('msg', type=str, help='message to be encrypted or decrypted')
args = parser.parse_args()
if args.encrypt and args.decrypt:
    parser.print_usage(file=sys.stderr)
    print('-e and -d options are not allowed together. Please, use -e or -d', file=sys.stderr)
    exit(1)
if not args.encrypt and not args.decrypt:
    parser.print_usage(file=sys.stderr)
    print('-e or -d option must be present', file=sys.stderr)
    exit(1)
if args.decrypt and len(args.msg) < 32:
    print('Bad message', file=sys.stderr)
    exit(11)


backend = default_backend()
key = b''
try:
    if len(args.key) != 32 and len(args.key) != 64:
        raise Exception
    key = bytes.fromhex(args.key)
except:
    print('Key must be hexadecimal encoded sequence of 16 or 32 bytes', file=sys.stderr)
    exit(10)

nonce = b''
msg = b''
try:
    if args.encrypt:
        nonce = os.urandom(16)
        msg = bytes(args.msg, encoding='utf-8')
    elif args.decrypt:
        nonce = bytes.fromhex(args.msg[-32:])
        msg = bytes.fromhex(args.msg[0:-32])
except:
    print('Bad message', file=sys.stderr)
    exit(12)

cipher = Cipher(algorithms.AES(key), modes.CTR(nonce), backend=backend)

if args.encrypt:
    encryptor = cipher.encryptor()
    encrypted = encryptor.update(msg) + encryptor.finalize()
    print(encrypted.hex() + nonce.hex())

elif args.decrypt:
    decryptor = cipher.decryptor()
    decrypted = decryptor.update(msg) + decryptor.finalize()
    decrypted_str = ''
    try:
        decrypted_str = decrypted.decode('utf-8')
    except:
        print('Bad key or message', file=sys.stderr)
        exit(20)
    print(decrypted_str)
