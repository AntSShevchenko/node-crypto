#!/usr/bin/env node

// AES-256-CTR
// ./sym-crypto.js -e --key=8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92 'Билли Бонс умер.'
// ./sym-crypto.js -d --key=8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92 0876e0013929e1490bf4fdd89b716fd2a2912bee50f82bcd6b13c7893bf7f3611c07d91306ab99c7d76445dcc1

// AES-128-CTR
// ./sym-crypto.js -e --key=8d969eef6ecad3c29a3a629280e686cf 'Билли Бонс умер.'
// ./sym-crypto.js -d --key=8d969eef6ecad3c29a3a629280e686cf 2a09ba9a944ef933fed0524a95867cefac963128f44314cbc19f088922b8a6b21f7f90eb2f8c63676d8f050a21

const crypto = require('crypto');
const argv = require('yargs').argv;

let key;
try {
    if (!argv.key || (argv.key.length !== 32 && argv.key.length !== 64)) {
        throw Error('')
    }
    key = Buffer.from(argv.key, 'hex');
    if (key.length !== 16 && key.length !== 32) {
        throw Error('')
    }
} catch(e) {
    console.error('ERROR: Key must be hexadecimal encoded sequence of 16 or 32 bytes');
    return;
}

const algo = key.length === 16 ? 'AES-128-CTR' : 'AES-256-CTR';
const result = [];

if (argv.e) {
    // Encryption
    const nonce_array = Array.from({length: 16}, () => Math.floor(Math.random() * 256));
    const iv = Buffer.from(nonce_array);
    const cipher = crypto.createCipheriv(algo, key, iv);
    argv._.forEach( function (phrase) {
        result.push(cipher.update(phrase, 'utf8', 'hex'));
    });
    result.push(cipher.final('hex'));
    result.push(iv.toString('hex'));

} else if (argv.d && argv.key) {
    // Decryption
    let msg = argv._.join('');
    if (msg.length < 32) {
        console.error('ERROR: Bad message');
        return;
    }
    const iv = Buffer.from(msg.slice(-32), 'hex');
    msg = msg.slice(0, -32);
    const decipher = crypto.createDecipheriv(algo, key, iv);
    try {
        result.push(decipher.update(msg, 'hex', 'utf8'));
        result.push(decipher.final('utf8'));
    } catch (e) {
        console.error('ERROR: Bad message');
        return;
    }
}
console.log(result.join(''));
